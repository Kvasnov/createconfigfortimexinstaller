import os
import os.path

Timex = "Timex.cfg"
TimexMilestone = "TimexMilestone.cfg"
dir = "D:\Temp\TimexTemp\TimexInstallDir"
MIPSDK = "D:\Temp\TimexTemp\TimexInstallDir\Plugins\MIPSDK"

filesTimex = list()
filesTimexMilestone = list()

with open( Timex, "w", encoding='utf-16' ) as file:
    file.write(";aic")
    file.write("\nSetCurrentFeature AlwaysInstall")

with open( TimexMilestone, "w", encoding='utf-16' ) as file:
    file.write(";aic")
    file.write("\nSetCurrentFeature MainFeature")

for root, dirs, files in os.walk(dir): 
    for file in files:

        if file.startswith("Milestone"):
            filesTimexMilestone.append("\nAddFile TIMEXDIR" +  root[len(dir):len(root)] + " "+ "\"" + os.path.join(root, file)+ "\"")

        elif MIPSDK == root:
            filesTimexMilestone.append("\nAddFile TIMEXDIR\\" + " "+ "\"" + os.path.join(root, file)+ "\"")

        elif not file.startswith("Milestone") and not MIPSDK == root and not dir == root:
            filesTimex.append("\nAddFile APPDIR" + root[len(dir):len(root)] + " "+ "\"" + os.path.join(root, file)+ "\"")

        elif (file.endswith(".dll") or file.endswith(".config")) and dir == root:
            filesTimex.append("\nAddFile APPDIR" + " " + "\"" + os.path.join(root, file)+ "\"")

with open(Timex, "a", encoding='utf-16' ) as file:
    for item in filesTimex:
        file.write(item)
    file.write("\nrebuild -buildslist DefaultBuild")

with open(TimexMilestone, "a", encoding='utf-16' ) as file:
    for item in filesTimexMilestone:
        file.write(item)
    file.write("\nrebuild -buildslist DefaultBuild")